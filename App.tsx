import 'react-native-gesture-handler';
import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect } from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import AsyncStorage from '@react-native-async-storage/async-storage';


import useCachedResources from './hooks/useCachedResources';
import useColorScheme from './hooks/useColorScheme';
import Navigation from './navigation';

import todoService from "./service/todos.service";
import albumsService from './service/albums.service';
import postsService from './service/posts.service';

export default function App() {
  const isLoadingComplete = useCachedResources();
  const colorScheme = useColorScheme();
  const [ hasError, setHasError ] = useState(false)


  useEffect(() => {
    getAlbums()
    getTodos()
    getPosts()
  }, []);

  const getTodos: any = async () => {
    try {
      const value = await AsyncStorage.getItem('@todo')
      if(value === null) {
        console.log("here")
        const { data } = await todoService.getTodos();
        await AsyncStorage.setItem('@todos', JSON.stringify(data))
      }
      setHasError(false)
    } catch (err) {
      setHasError(true)
    }
  }

  const getPosts: any = async () => {
    try {
      const value = await AsyncStorage.getItem('@posts')
      if (value === null){
        const { data } = await postsService.getPosts();
        await AsyncStorage.setItem('@posts', JSON.stringify(data))
      }
      
      setHasError(false)
    } catch (err) {
      setHasError(true)
    }
  }

  const getAlbums: any = async () => {
    try {
      const value = await AsyncStorage.getItem('@albums')
      if (value === null){
        const { data } = await albumsService.getAlbums();
        await AsyncStorage.setItem('@albums', JSON.stringify(data))
      }
      setHasError(false)
    } catch (err) {
      setHasError(true)
    }
  }

  if (!isLoadingComplete) {
    return null;
  } else {
    return (
      <SafeAreaProvider>
        <Navigation colorScheme={colorScheme} />
        <StatusBar />
      </SafeAreaProvider>
    );
  }
}
