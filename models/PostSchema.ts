export default{
  name: "Post",
  properties: {
    _id: "int",
    userId: "int",
    id: "int",
    title: "string",
    body: "string",
  },
  primaryKey: "_id",
};