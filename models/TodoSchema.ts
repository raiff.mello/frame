export default{
  name: "Todo",
  properties: {
    _id: "int",
    userId: "int",
    id: "int",
    title: "string",
    completed: "boolean",
  },
  primaryKey: "_id",
};