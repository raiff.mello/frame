export default{
  name: "Album",
  properties: {
    _id: "int",
    userId: "int",
    id: "int",
    title: "string",
  },
  primaryKey: "_id",
};