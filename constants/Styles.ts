import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  item: {
    borderRadius:5,
    padding: 10,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  todoItem:{
    backgroundColor: '#35bacc',
  },
  postItem:{
    backgroundColor: '#95ee7f',
  },
  albumItem:{
    backgroundColor: '#f3c88f',
  }
})