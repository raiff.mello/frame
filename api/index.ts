import axios from 'axios';

// fetch('https://jsonplaceholder.typicode.com/todos/1')
//   .then(response => response.json())
//   .then(json => console.log(json))

export const BASE_URL = 'https://jsonplaceholder.typicode.com';
const api = axios.create({ baseURL: BASE_URL });

api.interceptors.response.use(
    (response) => response,
    (error) => Promise.reject((error.response && error.response.data) || 'Something went wrong')
);

export default api;