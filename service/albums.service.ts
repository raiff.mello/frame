import api from "../api";

export class AlbumsService {
    private static instance: AlbumsService;

    constructor() {
        if (!AlbumsService.instance) {
            AlbumsService.instance = this
        }
        return AlbumsService.instance
    }

    getAlbums = () => {
        return api.get(`/albums/`)
    }
}

const albumsService = new AlbumsService()
export default albumsService