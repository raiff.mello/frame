import api from "../api";

export class PostsService {
    private static instance: PostsService;

    constructor() {
        if (!PostsService.instance) {
            PostsService.instance = this
        }
        return PostsService.instance
    }

    getPosts = () => {
        return api.get(`/posts/`)
    }
}

const postsService = new PostsService()
export default postsService