import api from "../api";

export class TodosService {
    private static instance: TodosService;

    constructor() {
        if (!TodosService.instance) {
            TodosService.instance = this
        }
        return TodosService.instance
    }

    getTodos = () => {
        return api.get(`/todos/`)
    }
}

const todosService = new TodosService()
export default todosService