import React, { useState, useEffect }  from 'react';
import { FlatList, SafeAreaView } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { Text, View } from '../components/Themed';
import Styles from '../constants/Styles';

export default function TabThreeScreen() {
  const [ albums, setAlbums ] = useState<{user:number, id: number, title: string}[]>([])
  
  useEffect(() => {
    setData()
  },[]);

  const setData: any = async () => {
    try {
      const value = await AsyncStorage.getItem('@albums')
      if(value !== null) {
        setAlbums(JSON.parse(value))
      }
    } catch (err) {
      setAlbums([])
    }
  }

  const Item = ({ title }) => (
    <View style={[Styles.item, Styles.albumItem]}>
    <Text style={Styles.title}>{title}</Text>
  </View>
  );

  const renderItem = ({item}) => (
    <Item title={item.title} />
  );

  return (
    <View style={Styles.container}>
      <SafeAreaView style={Styles.container}>
        <FlatList
          data={albums}
          renderItem={renderItem}
          keyExtractor={item => item.id.toString()}
        />
      </SafeAreaView>
    </View>
  );
}
