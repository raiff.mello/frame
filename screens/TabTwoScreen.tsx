import React, { useState, useEffect }  from 'react';
import { FlatList, SafeAreaView } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { Text, View } from '../components/Themed';
import Styles from '../constants/Styles';


export default function TabTwoScreen() {
  const [ posts, setPosts ] = useState<{user:number, id: number, title: string, body: string}[]>([])
  
  useEffect(() => {
    setData()
  },[]);

  const setData: any = async () => {
    try {
      const value = await AsyncStorage.getItem('@posts')
      if(value !== null) {
        setPosts(JSON.parse(value))
      }
    } catch (err) {
      setPosts([])
    }
  }

  const Item = ({ title }) => (
    <View style={[Styles.item, Styles.postItem]}>
    <Text style={Styles.title}>{title}</Text>
  </View>
  );

  const renderItem = ({item}) => (
    <Item title={item.title} />
  );

  return (
    <View style={Styles.container}>
      <SafeAreaView style={Styles.container}>
        <FlatList
          data={posts}
          renderItem={renderItem}
          keyExtractor={item => item.id.toString()}
        />
      </SafeAreaView>
    </View>
  );
}
