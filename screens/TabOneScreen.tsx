import React, { useState, useEffect }  from 'react';
import { FlatList, SafeAreaView } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { Text, View } from '../components/Themed';
import Styles from '../constants/Styles';

export default function TabOneScreen() {
  const [ todos, setTodos ] = useState<{user:number, id: number, title: string, completed: boolean}[]>([])

  useEffect(() => {
    setData()
  }, []);

  const setData: any = async () => {
    try {
      const value = await AsyncStorage.getItem('@todos')
      if(value !== null) {
        setTodos(JSON.parse(value))
      }
    } catch (err) {
      setTodos([])
    }
  }

  const Item = ({ title }) => (
    <View style={[Styles.item, Styles.todoItem]}>
    <Text style={Styles.title}>{title}</Text>
  </View>
  );

  const renderItem = ({item}) => (
    <Item title={item.title} />
  );

  return (
    <View style={Styles.container}>
      <SafeAreaView style={Styles.container}>
        <FlatList
          data={todos}
          renderItem={renderItem}
          keyExtractor={item => item.id.toString()}
        />
      </SafeAreaView>
    </View>
  );
}

